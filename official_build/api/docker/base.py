'''
Docker
'''
import requests


class DockerHubAPIService:

    def get_size(self, docker_image_path: str) -> int:
        array = docker_image_path.split(':')
        tag = "latest"
        docker_image = array[0]
        if len(array) == 2:
            tag = array[1]
        request = requests.get(f"https://hub.docker.com/v2/repositories/{docker_image}/tags/{tag}")
        request.raise_for_status()
        request.close()
        size = request.json()['full_size']
        return int(size)
